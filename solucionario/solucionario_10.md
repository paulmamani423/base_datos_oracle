# Ejercicio 01 //Una farmacia guarda información referente a sus medicamentos en una tabla llamada "medicamentos".//

# Elimine la tabla y créela con la siguiente estructura:

codigo sql
```sql
 drop table medicamentos;
 create table medicamentos(
    codigo number(5) not null,
    nombre varchar2(20) not null,
    laboratorio varchar2(20),
    precio number(5,2),
    cantidad number(3,0) not null
);
```
--- Visualice la estructura de la tabla "medicamentos" note que los campos "codigo", "nombre" y "cantidad", en la columna "Null" muestra "NOT NULL".


--- Ingrese algunos registros con valores "null" para los campos que lo admitan:
codigo sql
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'Sertal gotas',null,null,100); 
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(2,'Sertal compuesto',null,8.90,150);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(3,'Buscapina','Roche',null,200);
```
--- Vea todos los registros.
salida 
```sql
select * from medicamentos;
CODIGO |     NOMBRE      |  LABORATORIO  | PRECIO | CANTIDAD
-----------------------------------------------------------
   1   | Sertal gotas    |              |        |  100
   2   | Sertal compuesto|              |  8.90  |  150
   3   | Buscapina       | Roche        |        |  200

--- Ingrese un registro con valor "0" para el precio y cadena vacía para el laboratorio.

insert into medicamentos (codigo, nombre, laboratorio, precio, cantidad) values (4, 'Medicamento X', '', 0, 50);

--- Intente ingresar un registro con cadena vacía para el nombre (mensaje de error)

insert into medicamentos (codigo, nombre, laboratorio, precio, cantidad) values (5, '', 'Laboratorio Y', 5.50, 75);

--- Intente ingresar un registro con valor nulo para un campo que no lo admite (aparece un mensaje de error)

insert into medicamentos (codigo, nombre, laboratorio, precio, cantidad) values (6, 'Medicamento Z', 'Laboratorio Z', null, 100);

--- Ingrese un registro con una cadena de 1 espacio para el laboratorio.

insert into medicamentos (codigo, nombre, laboratorio, precio, cantidad) values (7, 'Medicamento A', ' ', 10.99, 120);

--- Recupere los registros cuyo laboratorio contenga 1 espacio (1 registro)

select * from medicamentos where laboratorio like '% %';

CODIGO |   NOMBRE    | LABORATORIO | PRECIO | CANTIDAD
-----------------------------------------------------
   7   | Medicamento A|             | 10.99  |  120

--- Recupere los registros cuyo laboratorio sea distinto de ' '(cadena de 1 espacio) (1 registro)

select * from medicamentos where laboratorio <> ' ';

CODIGO |     NOMBRE      |  LABORATORIO  | PRECIO | CANTIDAD
-----------------------------------------------------------
   1   | Sertal gotas    |              |        |  100
   2   | Sertal compuesto|              |  8.90  |  150
   3   | Buscapina       | Roche        |        |  200
   4   | Medicamento X   |              |  0.00  |   50
   5   |                 | Laboratorio Y |  5.50  |   75
   6   | Medicamento Z   | Laboratorio Z |        |  100
   7   | Medicamento A   |              | 10.99  |  120


--- Ejercicio 02 //Trabaje con la tabla que almacena los datos sobre películas, llamada "peliculas".//

--- Elimine la tabla:

codigo sql
drop table agenda;
salida sql 
TABLE "peliculas" eliminada;

--- Créela con la siguiente estructura:

create table peliculas(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    actor varchar2(20),
    duracion number(3)
);

--- Visualice la estructura de la tabla. note que el campo "codigo" y "titulo", en la columna "Null" muestran "NOT NULL".

desc peliculas;

Name    |  Null | Type
---------------------
CODIGO  | NOT NULL | NUMBER(4)
TITULO  | NOT NULL | VARCHAR2(40)
ACTOR   |          | VARCHAR2(20)
DURACION|          | NUMBER(3)

--- Ingrese los siguientes registros:

insert into peliculas (codigo,titulo,actor,duracion) values(1,'Mision imposible','Tom Cruise',120);
insert into peliculas (codigo,titulo,actor,duracion) values(2,'Harry Potter y la piedra filosofal',null,180);
insert into peliculas (codigo,titulo,actor,duracion) values(3,'Harry Potter y la camara secreta','Daniel R.',null);
insert into peliculas (codigo,titulo,actor,duracion) values(0,'Mision imposible 2','',150);
insert into peliculas (codigo,titulo,actor,duracion) values(4,'Titanic','L. Di Caprio',220);
insert into peliculas (codigo,titulo,actor,duracion) values(5,'Mujer bonita','R. Gere.J. Roberts',0);

CODIGO |           TITULO            |         ACTOR         | DURACION
---------------------------------------------------------------------
   1   |       Mision imposible      |      Tom Cruise       |   120
   2   | Harry Potter y la piedra... |                       |   180
   3   | Harry Potter y la camara... |       Daniel R.       |        
   0   |    Mision imposible 2       |                       |   150
   4   |          Titanic            |     L. Di Caprio      |   220
   5   |        Mujer bonita         | R. Gere.J. Roberts    |     0

--- Recupere todos los registros para ver cómo Oracle los almacenó.

select * from peliculas;
CODIGO |           TITULO            |         ACTOR         | DURACION
---------------------------------------------------------------------
   1   |       Mision imposible      |      Tom Cruise       |   120
   2   | Harry Potter y la piedra... |                       |   180
   3   | Harry Potter y la camara... |       Daniel R.       |        
   0   |    Mision imposible 2       |                       |   150
   4   |          Titanic            |     L. Di Caprio      |   220
   5   |        Mujer bonita         | R. Gere.J. Roberts    |   NULL


--- Intente ingresar un registro con valor nulo para campos que no lo admiten (aparece un mensaje de error)

insert into peliculas (codigo, titulo, actor, duracion) values (6, null, null, null);

ORA-01400: no se puede realizar una inserción NULL en ("PELICULAS"."CODIGO")

--- Muestre todos los registros.

select * from peliculas;

CODIGO |           TITULO            |         ACTOR         | DURACION
---------------------------------------------------------------------
   1   |       Mision imposible      |      Tom Cruise       |   120
   2   | Harry Potter y la piedra...

--- Actualice la película en cuyo campo "duracion" hay 0 por "null" (1 registro)

update peliculas set duracion = null where duracion = 0;

--- Recupere todos los registros.

select * from peliculas;

CODIGO |           TITULO            |         ACTOR         | DURACION
---------------------------------------------------------------------
   1   |       Mision imposible      |      Tom Cruise       |   120
   2   | Harry Potter y la piedra... |                       |   180
   3   | Harry Potter y la camara... |       Daniel R.       |        
   0   |    Mision imposible 2       |                       |   150
   4   |          Titanic            |     L. Di Caprio      |   220
   5   |        Mujer bonita         | R. Gere.J. Roberts    |   NULL
