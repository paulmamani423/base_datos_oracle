## Ejercicio 01 //Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla con ese nombre.//

## Elimine "articulos"

codigo sql
```sql
DROP TABLE articulos;
salida sql
TABLE articulos dropped;

-- Cree la tabla, con la siguiente estructura:

codigo sql
drop table if exists articulos;
create table articulos(
    codigo number(5),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(6,2),
    cantidad number(3)
);

-- Ingrese algunos registros:

insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (1,'impresora','Epson Stylus C45',400.80,20);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (2,'impresora','Epson Stylus C85',500,30);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (3,'monitor','Samsung 14',800,10);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (4,'teclado','ingles Biswal',100,50);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (5,'teclado','español Biswal',90,50);

-- Seleccione los datos de las impresoras (2 registros) 

SELECT * FROM articulos WHERE nombre = 'impresora';

CODIGO | NOMBRE     | DESCRIPCION       | PRECIO | CANTIDAD
-------|------------|------------------|--------|---------
1      | impresora  | Epson Stylus C45  | 400.80 | 20
2      | impresora  | Epson Stylus C85  | 500    | 30

-- Seleccione los artículos cuyo precio sea mayor o igual a 400 (3 registros)

SELECT * FROM articulos WHERE precio >= 400;

CODIGO | NOMBRE     | DESCRIPCION      | PRECIO | CANTIDAD
-------|------------|-----------------|--------|---------
1      | impresora  | Epson Stylus C45 | 400.80 | 20
2      | impresora  | Epson Stylus C85 | 500    | 30
3      | monitor    | Samsung 14       | 800    | 10

--Seleccione el código y nombre de los artículos cuya cantidad sea menor a 30 (2 registros)

SELECT codigo, nombre FROM articulos WHERE cantidad < 30;

CODIGO | NOMBRE  
-------|----------
1      | impresora
3      | monitor

-- Selecciones el nombre y descripción de los artículos que NO cuesten $100 (4 registros)

SELECT nombre, descripcion FROM articulos WHERE precio <> 100;

NOMBRE     | DESCRIPCION       
------------|------------------
impresora   | Epson Stylus C45  
impresora   | Epson Stylus C85  
monitor     | Samsung 14        
teclado     | ingles Biswal     


--Ejercicio 02 //Un video club que alquila películas en video almacena la información de sus películas en alquiler en una tabla denominada "peliculas".//

--Elimine la tabla.
codigo sql
drop table peliculas;
salida sql 
TABLE "peliculas" eliminada;

--Cree la tabla elijiendo el tipo de dato adecuado para cada campo.
codigo sql
drop table if exists peliculas;
create table peliculas(
    titulo varchar2(20),
    actor varchar2(20),
    duracion number(3),
    cantidad number(1)
);

--Ingrese los siguientes registros.

insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',120,3);
insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',180,4);
insert into peliculas (titulo, actor, duracion, cantidad) values ('Mujer bonita','Julia R.',90,1);
insert into peliculas (titulo, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',80,2);

-- Seleccione las péliculas cuya duracion no supere los 90 minutos (2 registros)

SELECT * FROM peliculas WHERE duracion <= 90;

TITULO            | ACTOR          | DURACION | CANTIDAD
------------------|----------------|----------|---------
Mujer bonita      | Julia R.       | 90       | 1
Elsa y Fred       | China Zorrilla | 80       | 2


-- Seleccione el tituylo de todas las peliculas en las que el actor NO sea "TOm Cruise" (2 registros)

SELECT titulo FROM peliculas WHERE actor <> 'Tom Cruise';

TITULO            |
------------------|
Mujer bonita      |
Elsa y Fred       |


-- Muestre todos los campos, exepto "duracion", de todas las peliculas de las que haya mas de 2 copias (2 registros)

SELECT titulo, actor, cantidad FROM peliculas WHERE cantidad > 2;

TITULO            | ACTOR          | CANTIDAD
------------------|----------------|---------
Mision imposible  | Tom Cruise     | 3
Mision imposible 2| Tom Cruise     | 4
