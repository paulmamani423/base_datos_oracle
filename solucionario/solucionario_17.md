# Ejercico 01
# Un comercio que tiene un stand en una feria registra en una tabla llamada "visitantes" algunos datos de las personas que visitan o compran en su stand para luego enviarle publicidad de sus productos.


# Elimine la tabla "visitantes"
codigo
```sql
DROP TABLE visitantes;

```


# Cree la tabla con la siguiente estructura:

codigo
```sql
create table visitantes(
    nombre varchar2(30),
    edad number(2),
    sexo char(1) default 'f',
    domicilio varchar2(30),
    ciudad varchar2(20) default 'Cordoba',
    telefono varchar(11),
    mail varchar(30) default 'no tiene',
    montocompra number (6,2)
);
```



# Analice la información que retorna la siguiente consulta:
codigo
```sql
select column_name,nullable,data_default
from user_tab_columns where TABLE_NAME = 'VISITANTES';

```



# Todos los campos admiten valores nulos; hay 3 campos con valores predeterminados.

# Ingrese algunos registros sin especificar valores para algunos campos para ver cómo opera la cláusula "default":
codigo
```sql
insert into visitantes (domicilio,ciudad,telefono,mail,montocompra)
values ('Colon 123','Cordoba','4334455','<juanlopez@hotmail.com>',59.80);
insert into visitantes (nombre,edad,sexo,telefono,mail,montocompra)
values ('Marcos Torres',29,'m','4112233','<marcostorres@hotmail.com>',60);
insert into visitantes (nombre,edad,sexo,domicilio,ciudad)
values ('Susana Molina',43,'f','Bulnes 345','Carlos Paz');
```



# Recupere todos los registros.
codigo
```sql
SELECT * FROM visitantes;

```
salida
```sql
NOMBRE            EDAD    SEXO    DOMICILIO      CIUDAD      TELEFONO    MAIL                      MONTOCOMPRA
-------------------------------------------------------------------------------
Colon 123         null    f       Colon 123      Cordoba     4334455     <juanlopez@hotmail.com>    59.80
Marcos Torres     29      m       null           null        4112233     <marcostorres@hotmail.com> 60
Susana Molina     43      f       Bulnes 345     Carlos Paz  null        no tiene                  null


```
# Los campos de aquellos registros para los cuales no se ingresó valor almacenaron el valor por defecto ("null" o el especificado con "default").
# Use la palabra "default" para ingresar valores en un "insert"
codigo
```sql
INSERT INTO visitantes (nombre, edad, domicilio, ciudad, montocompra)
VALUES ('Laura Lopez', 35, DEFAULT, DEFAULT, DEFAULT);

```


# Recupere el registro anteriormente ingresado.
codigo
```sql
SELECT * FROM visitantes WHERE nombre = 'Laura Lopez';

```
salida
```sql
NOMBRE        EDAD    SEXO    DOMICILIO  CIUDAD   TELEFONO    MAIL          MONTOCOMPRA
-------------------------------------------------------------------------------
Laura Lopez   null    f       null       Cordoba  null        no tiene      null


```