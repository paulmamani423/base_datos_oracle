# Ejercicio 01
# Un banco tiene registrados las cuentas corrientes de sus clientes en una tabla llamada "cuentas".
# La tabla contiene estos datos:
codigo
```sql
 Número de Cuenta        Documento       Nombre          Saldo
 ______________________________________________________________
 1234                         25666777        Pedro Perez     500000.60
 2234                         27888999        Juan Lopez      -250000
 3344                         27888999        Juan Lopez      4000.50
 3346                         32111222        Susana Molina   1000

```


# Elimine la tabla "cuentas":
codigo
```sql
DROP TABLE cuentas;

```

# Cree la tabla eligiendo el tipo de dato adecuado para almacenar los datos descriptos arriba:

# Número de cuenta: entero hasta 9999, no nulo, no puede haber valores repetidos, clave primaria;

# Documento del propietario de la cuenta: cadena de caracteres de 8 de longitud (siempre 8), no nulo;

# Nombre del propietario de la cuenta: cadena de caracteres de 30 de longitud,

# Saldo de la cuenta: valores que no superan 999999.99

codigo
```sql
create table cuentas(
    numero number(4) not null,
    documento char(8),
    nombre varchar2(30),
    saldo number(8,2),
    primary key (numero)
);

```



# Ingrese los siguientes registros:

codigo
```sql
insert into cuentas(numero,documento,nombre,saldo) values('1234','25666777','Pedro Perez',500000.60);
insert into cuentas(numero,documento,nombre,saldo) values('2234','27888999','Juan Lopez',-250000);
insert into cuentas(numero,documento,nombre,saldo) values('3344','27888999','Juan Lopez',4000.50);
insert into cuentas(numero,documento,nombre,saldo) values('3346','32111222','Susana Molina',1000);
```


# Note que hay dos cuentas, con distinto número de cuenta, de la misma persona.

# Seleccione todos los registros cuyo saldo sea mayor a "4000" (2 registros)

codigo
```sql
SELECT * FROM cuentas WHERE saldo > 4000;

```
salida
```sql
| numero | documento | nombre      | saldo     |
|--------|-----------|-------------|-----------|
| 1234   | 25666777  | Pedro Perez | 500000.60 |
| 3344   | 27888999  | Juan Lopez  | 4000.50   |


```
# Muestre el número de cuenta y saldo de todas las cuentas cuyo propietario sea "Juan Lopez" (2 registros)

codigo
```sql
SELECT numero, saldo FROM cuentas WHERE nombre = 'Juan Lopez';

```
salida
```sql
| numero | saldo     |
|--------|-----------|
| 2234   | -250000.00|
| 3344   | 4000.50   |


```
# Muestre las cuentas con saldo negativo (1 registro)
codigo
```sql
SELECT * FROM cuentas WHERE saldo < 0;

```
salida
```sql
| numero | documento | nombre      | saldo     |
|--------|-----------|-------------|-----------|
| 2234   | 27888999  | Juan Lopez  | -250000.00|


```

# Muestre todas las cuentas cuyo número es igual o mayor a "3000" (2 registros)
codigo
```sql
SELECT * FROM cuentas WHERE numero >= 3000;

```
salida
```sql
| numero | documento | nombre          | saldo    |
|--------|-----------|-----------------|----------|
| 3344   | 27888999  | Juan Lopez      | 4000.50  |
| 3346   | 32111222  | Susana Molina   | 1000.00  |


```