# Ejercicios propuestos
# Primer problema:
# Un banco tiene registrados las cuentas corrientes de sus clientes en una tabla llamada "cuentas".

# Elimine la tabla "cuentas":

codigo
```sql
drop table cuentas;
```


# Cree la tabla :

codigo
```sql
create table cuentas(
    numero number(10) not null,
    documento char(8) not null,
    nombre varchar2(30),
    saldo number(9,2)
);
```


# Ingrese un registro con valores para todos sus campos, omitiendo la lista de campos.
codigo
```sql
INSERT INTO cuentas VALUES (1234567890, '12345678', 'John Doe', 5000.00);

```

# Ingrese un registro omitiendo algún campo que admita valores nulos.
codigo
```sql
INSERT INTO cuentas (numero, documento, saldo) VALUES (9876543210, '87654321', 10000.00);

```

# Verifique que en tal campo se almacenó "null"

# Intente ingresar un registro listando 3 campos y colocando 4 valores. Un mensaje indica que hay demasiados valores.

codigo
```sql
INSERT INTO cuentas (numero, documento, nombre, saldo) VALUES (5555555555, '44444444', 'Jane Smith', 20000.00, 'Extra value');

```
# Intente ingresar un registro listando 3 campos y colocando 2 valores. Un mensaje indica que no hay suficientes valores.

codigo
```sql
INSERT INTO cuentas (numero, documento) VALUES (1111111111, '22222222');

```

# Intente ingresar un registro sin valor para un campo definido "not null".
codigo
```sql
INSERT INTO cuentas (numero, documento, nombre, saldo) VALUES (9999999999, '88888888', NULL, 15000.00);

```


# Vea los registros ingresados.
codigo
```sql
SELECT * FROM cuentas;

```
