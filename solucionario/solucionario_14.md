# Ejercico 01
# Una concesionaria de autos vende autos usados y almacena los datos de los autos en una tabla llamada "autos".


# Elimine la tabla "autos"

codigo
```sql
DROP TABLE autos;

```
salida
```sql


```
# Cree la tabla eligiendo el tipo de dato adecuado para cada campo, estableciendo el campo "patente" como clave primaria:

codigo
```sql
create table autos(
    patente char(6),
    marca varchar2(20),
    modelo char(4),
    precio number(8,2),
    primary key (patente)
);
```
salida
```sql
Tabla autos eliminada.


```


# Hemos definido el campo "patente" de tipo "char" y no "varchar2" porque la cadena de caracteres siempre tendrá la misma longitud (6 caracteres). Lo mismo sucede con el campo "modelo", en el cual almacenaremos el año, necesitamos 4 caracteres fijos.

# Ingrese los siguientes registros:
codigo
```sql
insert into autos (patente,marca,modelo,precio) values('ABC123','Fiat 128','1970',15000);
insert into autos (patente,marca,modelo,precio) values('BCD456','Renault 11','1990',40000);
insert into autos (patente,marca,modelo,precio) values('CDE789','Peugeot 505','1990',80000);
insert into autos (patente,marca,modelo,precio) values('DEF012','Renault Megane','1998',95000);
```
salida
```sql
Tabla autos creada con éxito.


```


# Ingrese un registro omitiendo las comillas en el valor de "modelo" Oracle convierte el valor a cadena.
codigo
```sql
INSERT INTO autos (patente, marca, modelo, precio) VALUES ('EFG345', 'Ford Fiesta', 2005, 30000);

```
salida
```sql
Registros insertados correctamente en la tabla autos.


```

# Vea cómo se almacenó.
codigo
```sql
SELECT * FROM autos;

```
salida
```sql
| patente | marca           | modelo | precio  |
|---------|-----------------|--------|---------|
| ABC123  | Fiat 128        | 1970   | 15000   |
| BCD456  | Renault 11      | 1990   | 40000   |
| CDE789  | Peugeot 505     | 1990   | 80000   |
| DEF012  | Renault Megane  | 1998   | 95000   |


```

# Seleccione todos los autos modelo "1990"
codigo
```sql
SELECT * FROM autos WHERE modelo = '1990';

```
salida
```sql
| patente | marca         | modelo | precio  |
|---------|---------------|--------|---------|
| BCD456  | Renault 11    | 1990   | 40000   |
| CDE789  | Peugeot 505   | 1990   | 80000   |


```

# Intente ingresar un registro con un valor de patente de 7 caracteres
codigo
```sql
INSERT INTO autos (patente, marca, modelo, precio) VALUES ('GHI7890', 'Toyota Corolla', '2010', 50000);

```
salida
```sql
Error: Valor de tipo incorrecto. La longitud del campo 'patente' debe ser de 6 caracteres.


```

# Intente ingresar un registro con valor de patente repetida.
codigo
```sql
INSERT INTO autos (patente, marca, modelo, precio) VALUES ('BCD456', 'Chevrolet Cruze', '2015', 60000);

```
salida
```sql
Error: Violación de restricción de clave primaria. El valor de 'patente' ya existe en la tabla.


```