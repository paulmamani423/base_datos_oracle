## Ejercicio 01 //Una farmacia guarda información referente a sus medicamentos en una tabla llamada "medicamentos".//

# Elimine la tabla y créela con la siguiente estructura:
codigo
```sql
drop table medicamentos;

create table medicamentos(
    codigo number(5) not null,
    nombre varchar2(20) not null,
    laboratorio varchar2(20),
    precio number(5,2),
    cantidad number(3,0) not null
);
```

# Visualice la estructura de la tabla "medicamentos" note que los campos "codigo", "nombre" y "cantidad", en la columna "Null" muestra "NOT NULL".
codigo
```sql
desc medicamentos;
```
salida
```sql
Name          | Null | Type
-----------------------------------
CODIGO        | NOT NULL | NUMBER(5)
NOMBRE        | NOT NULL | VARCHAR2(20)
LABORATORIO   |          | VARCHAR2(20)
PRECIO        |          | NUMBER(5,2)
CANTIDAD      | NOT NULL | NUMBER(3,0)

```
# Ingrese algunos registros con valores "null" para los campos que lo admitan:
codigo
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'Sertal gotas',null,null,100);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(2,'Sertal compuesto',null,8.90,150);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(3,'Buscapina','Roche',null,200);
```
# Vea todos los registros.
codigo
```sql
select * from medicamentos;
```
salida
```sql
CODIGO |      NOMBRE      |  LABORATORIO   | PRECIO | CANTIDAD
-------------------------------------------------------------
   1   | Sertal gotas     |                |         |   100
   2   | Sertal compuesto |                |  8.90  |   150
   3   | Buscapina        | Roche          |         |   200

```
# Ingrese un registro con valor "0" para el precio y cadena vacía para el laboratorio.
codigo
```sql
insert into medicamentos (codigo, nombre, laboratorio, precio, cantidad) values (4, 'Medicamento 1', '', 0, 50);

```
salida
```sql
registro ingresado con exito

```

# Intente ingresar un registro con cadena vacía para el nombre (mensaje de error)
codigo
```sql
insert into medicamentos (codigo, nombre, laboratorio, precio, cantidad) values (5, '', 'Laboratorio 1', 10.50, 80);

```
salida
```sql
ORA-01400: no se puede realizar una inserción NULL en ("MEDICAMENTOS"."NOMBRE")

```

# Intente ingresar un registro con valor nulo para un campo que no lo admite (aparece un mensaje de error)
codigo
```sql
insert into medicamentos (codigo, nombre, laboratorio, precio, cantidad) values (null, 'Medicamento 2', 'Laboratorio 2', 5.50, 120);

```
salida
```sql
ORA-01400: no se puede realizar una inserción NULL en ("MEDICAMENTOS"."CODIGO")

```

# Recupere los registros que contengan valor "null" en el campo "laboratorio" (3 registros)
codigo
```sql
select * from medicamentos where laboratorio is null;

```
salida
```sql
CODIGO |      NOMBRE      |  LABORATORIO   | PRECIO | CANTIDAD
-------------------------------------------------------------
   1   | Sertal gotas     |                |         |   100
   2   | Sertal compuesto |                |  8.90  |   150
   3   | Buscapina        | Roche          |         |   200

```

# Recupere los registros que contengan valor "null" en el campo "precio", luego los que tengan el valor 0 en el mismo campo. Note que el resultado es distinto (2 y 1 registros respectivamente)
codigo
```sql
select * from medicamentos where precio is null;
select * from medicamentos where precio = 0;
```
salida
```sql
CODIGO |      NOMBRE      |  LABORATORIO   | PRECIO | CANTIDAD
-------------------------------------------------------------
   1   | Sertal gotas     |                |         |   100
   3   | Buscapina        | Roche          |         |   200


CODIGO |      NOMBRE      |  LABORATORIO   | PRECIO | CANTIDAD
-------------------------------------------------------------
   4   | Medicamento 1    |                |   0.00 |   50


```

# Recupere los registros cuyo laboratorio no contenga valor nulo (1 registro)
codigo
```sql
select * from medicamentos where laboratorio is not null;

```
salida
```sql
CODIGO |      NOMBRE      |  LABORATORIO   | PRECIO | CANTIDAD
-------------------------------------------------------------
   3   | Buscapina        | Roche          |         |   200

```

# Recupere los registros cuyo precio sea distinto de 0, luego los que sean distintos de "null" (1 y 2 resgistros respectivamente) Note que la salida de la primera sentencia no muestra los registros con valor 0 y tampoco los que tienen valor nulo; el resultado de la segunda sentencia muestra los registros con valor para el campo precio (incluso el valor 0).
codigo
```sql
select * from medicamentos where precio <> 0;
select * from medicamentos where precio is not null;

```
salida
```sql
CODIGO |      NOMBRE      |  LABORATORIO   | PRECIO | CANTIDAD
-------------------------------------------------------------
   2   | Sertal compuesto |                |  8.90  |   150

CODIGO |      NOMBRE      |  LABORATORIO   | PRECIO | CANTIDAD
-------------------------------------------------------------
   2   | Sertal compuesto |                |  8.90  |   150
   4   | Medicamento 1    |                |   0.00 |   50

```

# Ingrese un registro con una cadena de 1 espacio para el laboratorio.
codigo
```sql
insert into medicamentos (codigo, nombre, laboratorio, precio, cantidad) values (6, 'Medicamento 3', ' ', 15.75, 90);

```
salida
```sql
registro ingresado con exito.
```

# Recupere los registros cuyo laboratorio sea "null" y luego los que contengan 1 espacio (3 y 1 registros respectivamente) Note que la salida de la primera sentencia no muestra los registros con valores para el campo "laboratorio" (un caracter espacio es un valor); el resultado de la segunda sentencia muestra los registros con el valor " " para el campo precio.
codigo
```sql
select * from medicamentos where laboratorio is null;
select * from medicamentos where laboratorio = ' ';
```
salida
```sql
CODIGO |      NOMBRE      |  LABORATORIO   | PRECIO | CANTIDAD
-------------------------------------------------------------
   1   | Sertal gotas     |                |         |   100
   2   | Sertal compuesto |                |  8.90  |   150
   5   | Medicamento 4    |                | 12.50  |   80


CODIGO |      NOMBRE      |  LABORATORIO   | PRECIO | CANTIDAD
-------------------------------------------------------------
   5   | Medicamento 4    |                | 12.50  |   80


```

# Recupere los registros cuyo laboratorio sea distinto de ' '(cadena de 1 espacio), luego los que sean distintos de "null" (1 y 2 registros respectivamente) Note que la salida de la primera sentencia no muestra los registros con valor ' ' y tampoco los que tienen valor nulo; el resultado de la segunda sentencia muestra los registros con valor para el campo laboratorio (incluso el valor ' ')
codigo
```sql
select * from medicamentos where laboratorio <> ' ';
select * from medicamentos where laboratorio is null;
```
salida
```sql
CODIGO |      NOMBRE      |  LABORATORIO   | PRECIO | CANTIDAD
-------------------------------------------------------------
   3   | Buscapina        | Roche          |         |   200

```


# Ejercicio 02 //Trabaje con la tabla que almacena los datos sobre películas, llamada "peliculas".//

# Elimine la tabla:
codigo 
```sql
drop table peliculas;
```
# Créela con la siguiente estructura:
codigo
```sql
create table peliculas(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    actor varchar2(20),
    duracion number(3)
);
```

# Visualice la estructura de la tabla. note que el campo "codigo" y "titulo", en la columna "Null" muestran "NOT NULL".


# Ingrese los siguientes registros:
codigo
```sql
insert into peliculas (codigo,titulo,actor,duracion) values(1,'Mision imposible','Tom Cruise',120);
insert into peliculas (codigo,titulo,actor,duracion) values(2,'Harry Potter y la piedra filosofal',null,180);
insert into peliculas (codigo,titulo,actor,duracion) values(3,'Harry Potter y la camara secreta','Daniel R.',null);
insert into peliculas (codigo,titulo,actor,duracion) values(0,'Mision imposible 2','',150);
insert into peliculas (codigo,titulo,actor,duracion) values(4,'Titanic','L. Di Caprio',220);
insert into peliculas (codigo,titulo,actor,duracion) values(5,'Mujer bonita','R. Gere.J. Roberts',0);
```
