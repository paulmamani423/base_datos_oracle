# Ejercicio 01 //Necesita almacenar los datos de amigos en una tabla.
# Los datos que guardará serán: apellido, nombre, domicilio y teléfono.


# Elimine la tabla "agenda" Si no existe, un mensaje indicará tal situación.
codigo
```sql
drop table medicamentos;

```

# Intente crear una tabla llamada "*agenda"

codigo
```sql
create table*agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);
```

# Cree una tabla llamada "agenda", debe tener los siguientes campos: apellido, varchar2(30); nombre, varchar2(20); domicilio, varchar2 (30) y telefono, varchar2(11)
codigo
```sql
create table*agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);
```


# Un mensaje indica que la tabla ha sido creada exitosamente.
codigo
```sql
SELECT COUNT(*) FROM user_tables WHERE table_name = 'AGENDA';
```


# Intente crearla nuevamente.
# Aparece mensaje de error indicando que el nombre ya lo tiene otro objeto.
codigo
```sql
SELECT table_name FROM all_tables;
```

# Visualice las tablas existentes (all_tables)
# La tabla "agenda" aparece en la lista.
codigo
```sql
DESCRIBE agenda;
```

# Visualice la estructura de la tabla "agenda" (describe)
salida
```sql
Name       Null?    Type
---------- -------- ------------
APELLIDO            VARCHAR2(30)
NOMBRE              VARCHAR2(20)
DOMICILIO           VARCHAR2(30)
TELEFONO            VARCHAR2(11)
```
# Aparece la siguiente tabla:
codigo
```sql
Name Null Type
-----------------------

APELLIDO VARCHAR2(30)
NOMBRE  VARCHAR2(20)
DOMICILIO VARCHAR2(30)
TELEFONO VARCHAR2(11)
```


# Ejercicio 02
# Necesita almacenar información referente a los libros de su biblioteca personal. Los datos que guardará serán: título del libro, nombre del autor y nombre de la editorial.
codigo
```sql
SELECT COUNT(*) FROM user_tables WHERE table_name = 'LIBROS';

```

# Elimine la tabla "libros"
# Si no existe, un mensaje indica tal situación.
codigo
```sql
DROP TABLE libros;
```
salida
```sql
COUNT(*)
--------
0

```
# Verifique que la tabla "libros" no existe (all_tables)
# No aparece en la lista.
codigo
```sql
SELECT COUNT(*) FROM user_tables WHERE table_name = 'LIBROS';
```
# Cree una tabla llamada "libros". Debe definirse con los siguientes campos: titulo, varchar2(20); autor, varchar2(30) y editorial, varchar2(15)
codigo
```sql
CREATE TABLE libros (
    titulo VARCHAR2(20),
    autor VARCHAR2(30),
    editorial VARCHAR2(15)
);
```
salida
```sql
Tabla LIBROS creada exitosamente.

```
# Intente crearla nuevamente:
# Aparece mensaje de error indicando que existe un objeto con el nombre "libros".
codigo
```sql
CREATE TABLE libros (
    titulo VARCHAR2(20),
    autor VARCHAR2(30),
    editorial VARCHAR2(15)
);
```
salida
```sql
Error: ORA-00955: nombre ya lo tiene otro objeto

```
# Visualice las tablas existentes
codigo
```sql
SELECT table_name FROM all_tables;
```
salida
```sql
TABLE_NAME
----------
LIBROS

```
# Visualice la estructura de la tabla "libros":
# Aparece "libros" en la lista.
codigo
```sql
DESCRIBE libros;

```
salida
```sql
Name       Null?    Type
---------- -------- ------------
TITULO              VARCHAR2(20)
AUTOR               VARCHAR2(30)
EDITORIAL           VARCHAR2(15)

```
# Elimine la tabla
codigo
```sql
DROP TABLE libros;
```
salida
```sql
Tabla LIBROS eliminada exitosamente.

```
# Intente eliminar la tabla
# Un mensaje indica que no existe.
codigo
```sql
DROP TABLE libros;
```

salida
```sql
Error: ORA-00942: tabla o vista no existe

```