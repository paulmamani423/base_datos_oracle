## Ejercicio 01 //Trabaje con la tabla "agenda" que almacena los datos de sus amigos.//

## Elimine la tabla y créela con la siguiente estructura:
codigo sql
```sql
drop table agenda;

create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

Nombre    | Null? | Tipo         |
---------------------------------
APELLIDO  |       | VARCHAR2(30) |
NOMBRE    |       | VARCHAR2(20) |
DOMICILIO |       | VARCHAR2(30) |
TELEFONO  |       | VARCHAR2(11) |

--Ingrese los siguientes registros:


insert into agenda (apellido,nombre,domicilio,telefono) values ('Acosta','Alberto','Colon 123','4234567');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Juarez','Juan','Avellaneda 135','4458787');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Lopez','Maria','Urquiza 333','4545454');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Lopez','Jose','Urquiza 333','4545454');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Suarez','Susana','Gral. Paz 1234','4123456');

APELLIDO | NOMBRE   | DOMICILIO        | TELEFONO
------------------------------------------------
Acosta   | Alberto  | Colon 123        | 4234567
Juarez   | Juan     | Avellaneda 135   | 4458787
Lopez    | Maria    | Urquiza 333      | 4545454
Lopez    | Jose     | Urquiza 333      | 4545454
Suarez   | Susana   | Gral. Paz 1234   | 4123456

--Modifique el registro cuyo nombre sea "Juan" por "Juan Jose" (1 registro actualizado)

update agenda set nombre = 'Juan Jose' where nombre = 'Juan';

APELLIDO | NOMBRE   | DOMICILIO        | TELEFONO
------------------------------------------------
Acosta   | Alberto  | Colon 123        | 4234567
Juarez   | Juan Jose| Avellaneda 135   | 4458787
Lopez    | Maria    | Urquiza 333      | 4545454
Lopez    | Jose     | Urquiza 333      | 4545454
Suarez   | Susana   | Gral. Paz 1234   | 4123456

--Actualice los registros cuyo número telefónico sea igual a "4545454" por "4445566" (2 registros)

update agenda set telefono = '4445566' where telefono = '4545454';

APELLIDO | NOMBRE   | DOMICILIO        | TELEFONO
------------------------------------------------
Acosta   | Alberto  | Colon 123        | 4234567
Juarez   | Juan Jose| Avellaneda 135   | 4458787
Lopez    | Maria    | Urquiza 333      | 4445566
Lopez    | Jose     | Urquiza 333      | 4445566
Suarez   | Susana   | Gral. Paz 1234   | 4123456

--Actualice los registros que tengan en el campo "nombre" el valor "Juan" por "Juan Jose" (ningún registro afectado porque ninguno cumple con la condición del "where")

update agenda set nombre = 'Juan Jose' where nombre = 'Juan';

No se realizaron cambios en la tabla, ya que no hay registros con el nombre "Juan".


--Ejercicio 02 //Trabaje con la tabla "libros" de una librería.//

--Elimine la tabla y créela con los siguientes campos: titulo (cadena de 30 caracteres de longitud), autor (cadena de 20), editorial (cadena de 15) y precio (entero no mayor a 999.99):

drop table libros;

create table libros (
    titulo varchar2(30),
    autor varchar2(20),
    editorial varchar2(15),
    precio number(5,2)
);

Nombre    | Null? | Tipo         |
---------------------------------
TITULO    |       | VARCHAR2(30) |
AUTOR     |       | VARCHAR2(20) |
EDITORIAL |       | VARCHAR2(15) |
PRECIO    |       | NUMBER(5,2)  |

--Ingrese los siguientes registros:

insert into libros (titulo, autor, editorial, precio) values ('El aleph','Borges','Emece',25.00);
insert into libros (titulo, autor, editorial, precio) values ('Martin Fierro','Jose Hernandez','Planeta',35.50);
insert into libros (titulo, autor, editorial, precio) values ('Aprenda PHP','Mario Molina','Emece',45.50);
insert into libros (titulo, autor, editorial, precio) values ('Cervantes y el quijote','Borges','Emece',25);
insert into libros (titulo, autor, editorial, precio) values ('Matematica estas ahi','Paenza','Siglo XXI',15);

TITULO                | AUTOR         | EDITORIAL  | PRECIO
---------------------------------------------------------
El aleph              | Borges        | Emece      | 25.00
Martin Fierro         | Jose Hernandez| Planeta    | 35.50
Aprenda PHP           | Mario Molina  | Emece      | 45.50
Cervantes y el quijote| Borges        | Emece      | 25.00
Matematica estas ahi  | Paenza        | Siglo XXI  | 15.00

--Muestre todos los registros (5 registros)

select * from libros;

TITULO                | AUTOR          | EDITORIAL   | PRECIO
----------------------------------------------------------
El aleph              | Borges         | Emece       | 25.00
Martin Fierro         | Jose Hernandez | Planeta     | 35.50
Aprenda PHP           | Mario Molina   | Emece       | 45.50
Cervantes y el quijote| Borges         | Emece       | 25.00
Matematica estas ahi  | Paenza         | Siglo XXI   | 15.00

--Modifique los registros cuyo autor sea igual a "Paenza", por "Adrian Paenza" (1 registro)

update libros set autor = 'Adrian Paenza' where autor = 'Paenza';

TITULO                | AUTOR          | EDITORIAL   | PRECIO
----------------------------------------------------------
El aleph              | Borges         | Emece       | 25.00
Martin Fierro         | Jose Hernandez | Planeta     | 35.50
Aprenda PHP           | Mario Molina   | Emece       | 45.50
Cervantes y el quijote| Borges         | Emece       | 25.00
Matematica estas ahi  | Adrian Paenza  | Siglo XXI   | 15.00

--Nuevamente, modifique los registros cuyo autor sea igual a "Paenza", por "Adrian Paenza" (ningún registro afectado porque ninguno cumple la condición)

update libros set autor = 'Adrian Paenza' where autor = 'Paenza';

TITULO                | AUTOR          | EDITORIAL   | PRECIO
----------------------------------------------------------
El aleph              | Borges         | Emece       | 25.00
Martin Fierro         | Jose Hernandez | Planeta     | 35.50
Aprenda PHP           | Mario Molina   | Emece       | 45.50
Cervantes y el quijote| Borges         | Emece       | 25.00
Matematica estas ahi  | Adrian Paenza  | Siglo XXI   | 15.00

--Actualice el precio del libro de "Mario Molina" a 27 pesos (1 registro)

update libros set precio = 27 where autor = 'Mario Molina';

TITULO                | AUTOR          | EDITORIAL   | PRECIO
----------------------------------------------------------
El aleph              | Borges         | Emece       | 25.00
Martin Fierro         | Jose Hernandez | Planeta     | 35.50
Aprenda PHP           | Mario Molina   | Emece       | 27.00
Cervantes y el quijote| Borges         | Emece       | 25.00
Matematica estas ahi  | Adrian Paenza  | Siglo XXI   | 15.00

--Actualice el valor del campo "editorial" por "Emece S.A.", para todos los registros cuya editorial sea igual a "Emece" (3 registros)

update libros set editorial = 'Emece S.A.' where editorial = 'Emece';

TITULO                | AUTOR          | EDITORIAL   | PRECIO
----------------------------------------------------------
El aleph              | Borges         | Emece S.A.  | 25.00
Martin Fierro         | Jose Hernandez | Planeta     | 35.50
Aprenda PHP           | Mario Molina   | Emece S.A.  | 27.00
Cervantes y el quijote| Borges         | Emece S.A.  | 25.00
Matematica estas ahi  | Adrian Paenza  | Siglo XXI   | 15.00
