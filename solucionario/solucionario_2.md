# Ejercicio 01
# Trabaje con la tabla "agenda" que almacena información de sus amigos.

# Elimine la tabla "agenda"
codigo
```sql
DROP TABLE agenda;

```

salida
```sql
Tabla AGENDA eliminada exitosamente.


```

# Cree una tabla llamada "agenda". Debe tener los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11)
codigo
```sql
CREATE TABLE agenda (
    apellido VARCHAR2(30),
    nombre VARCHAR2(20),
    domicilio VARCHAR2(30),
    telefono VARCHAR2(11)
);

```

salida
```sql
Tabla AGENDA creada exitosamente.


```
# Visualice las tablas existentes para verificar la creación de "agenda" (all_tables)

codigo
```sql
SELECT table_name FROM all_tables;

```

salida
```sql
TABLE_NAME
----------
AGENDA


```
# Visualice la estructura de la tabla "agenda" (describe)
codigo
```sql
DESCRIBE agenda;

```

salida
```sql
Name       Null?    Type
---------- -------- ------------
APELLIDO            VARCHAR2(30)
NOMBRE              VARCHAR2(20)
DOMICILIO           VARCHAR2(30)
TELEFONO            VARCHAR2(11)


```

# Ingrese los siguientes registros:

codigo
```sql
insert into agenda (apellido, nombre, domicilio, telefono) values ('Moreno','Alberto','Colon 123','4234567');
insert into agenda (apellido,nombre, domicilio, telefono) values ('Torres','Juan','Avellaneda 135','4458787');

```

# Seleccione todos los registros de la tabla.

codigo
```sql
SELECT * FROM agenda;


```

salida
```sql
APELLIDO  NOMBRE   DOMICILIO        TELEFONO
--------- -------- --------------- -----------
Moreno    Alberto  Colon 123        4234567
Torres    Juan     Avellaneda 135   4458787

AGENDA


```
# Elimine la tabla "agenda"

codigo
```sql
DROP TABLE agenda;


```

salida
```sql
Tabla AGENDA eliminada exitosamente.

AGENDA


```
# Intente eliminar la tabla nuevamente (aparece un mensaje de error)

codigo
```sql
DROP TABLE agenda;
```

salida
```sql
Error: ORA-00942: tabla o vista no existe

```