## Ejercicio 01 //Trabaje con la tabla "agenda" que registra la información referente a sus amigos.//

## Elimine la tabla.
codigo sql
```sql 
drop table agenda;
salida sql 
TABLE "peliculas" eliminada;

-- Cree la tabla con los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11):

codigo sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

-- Ingrese los siguientes registros (insert into):

insert into agenda(apellido,nombre,domicilio,telefono) values ('Alvarez','Alberto','Colon 123','4234567');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Juarez','Juan','Avellaneda 135','4458787');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez','Maria','Urquiza 333','4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez','Jose','Urquiza 333','4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Salas','Susana','Gral. Paz 1234','4123456');

apellido  | nombre   | domicilio       | telefono
-------------------------------------------------
Alvarez   | Alberto  | Colon 123       | 4234567
Juarez    | Juan     | Avellaneda 135  | 4458787
Lopez     | Maria    | Urquiza 333     | 4545454
Lopez     | Jose     | Urquiza 333     | 4545454
Salas     | Susana   | Gral. Paz 1234  | 4123456


-- Elimine el registro cuyo nombre sea "Juan" (1 registro)

DELETE FROM agenda WHERE nombre = 'Juan';

apellido  | nombre   | domicilio       | telefono
-------------------------------------------------
Alvarez   | Alberto  | Colon 123       | 4234567
Lopez     | Maria    | Urquiza 333     | 4545454
Lopez     | Jose     | Urquiza 333     | 4545454
Salas     | Susana   | Gral. Paz 1234  | 4123456


-- Elimine los registros cuyo número telefónico sea igual a "4545454" (2 registros)

DELETE FROM agenda WHERE telefono = '4545454';

apellido  | nombre   | domicilio       | telefono
-------------------------------------------------
Alvarez   | Alberto  | Colon 123       | 4234567
Salas     | Susana   | Gral. Paz 1234  | 4123456

-- Elimine todos los registros (2 registros)

DELETE FROM agenda;

La tabla "agenda" estará vacía, sin ningún registro.


--Ejercicio 02 //Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla con ese nombre.//

--Elimine "articulos"

codigo sql 
drop table articulos;
salida sql 
TABLE "articulos" eliminada;

--Cree la tabla, con la siguiente estructura:

codigo sql
create table articulos(
    codigo number(4,0),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(7,2),
    cantidad number(3)
);

Nombre        | Null? | Tipo         |
-------------------------------------
CODIGO        |       | NUMBER(4)    |
NOMBRE        |       | VARCHAR2(20) |
DESCRIPCION   |       | VARCHAR2(30) |
PRECIO        |       | NUMBER(7,2)  |
CANTIDAD      |       | NUMBER(3)    |

--Ingrese algunos registros:

insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (1,'impresora','Epson Stylus C45',400.80,20);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (2,'impresora','Epson Stylus C85',500,30);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (3,'monitor','Samsung 14',800,10);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (4,'teclado','ingles Biswal',100,50);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (5,'teclado','español Biswal',90,50);

CODIGO  | NOMBRE    | DESCRIPCION      | PRECIO  | CANTIDAD
----------------------------------------------------------
1       | impresora | Epson Stylus C45 | 400.80  | 20
2       | impresora | Epson Stylus C85 | 500.00  | 30
3       | monitor   | Samsung 14       | 800.00  | 10
4       | teclado   | ingles Biswal    | 100.00  | 50
5       | teclado   | español Biswal   | 90.00   | 50


--Elimine los artículos cuyo precio sea mayor o igual a 500 (2 registros)

DELETE FROM articulos WHERE precio >= 500;

CODIGO  | NOMBRE    | DESCRIPCION      | PRECIO  | CANTIDAD
----------------------------------------------------------
1       | impresora | Epson Stylus C45 | 400.80  | 20
4       | teclado   | ingles Biswal    | 100.00  | 50
5       | teclado   | español Biswal   | 90.00   | 50

--Elimine todas las impresoras (1 registro)

DELETE FROM articulos WHERE nombre = 'impresora';

CODIGO  | NOMBRE    | DESCRIPCION      | PRECIO  | CANTIDAD
----------------------------------------------------------
4       | teclado   | ingles Biswal    | 100.00  | 50
5       | teclado   | español Biswal   | 90.00   | 50

--Elimine todos los artículos cuyo código sea diferente a 4 (1 registro)

DELETE FROM articulos WHERE codigo <> 4;

CODIGO  | NOMBRE    | DESCRIPCION      | PRECIO  | CANTIDAD
----------------------------------------------------------
4       | teclado   | ingles Biswal    | 100.00  | 50

