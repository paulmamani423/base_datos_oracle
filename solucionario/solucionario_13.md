# Ejercicio de laboratorio
# Trabajamos con la tabla "libros" que almacena los datos de los libros de una librería.
# Eliminamos la tabla:
codigo
```sql
drop table libros;
```


# Creamos la tabla:


codigo
```sql
create table libros(
    codigo number(4),
    titulo varchar2(30),
    autor varchar2(20),
    editorial varchar2(15),
    precio number(5,2)
);
```