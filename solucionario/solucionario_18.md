# Ejercicios propuestos
# Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla con ese nombre.

# Elimine la tabla:

codigo
```sql
drop table articulos;
```

# Cree la tabla:
codigo
```sql
create table articulos(
    codigo number(4),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(8,2),
    cantidad number(3) default 0,
    primary key (codigo)
);

```


# Ingrese algunos registros:
codigo
```sql
insert into articulos
values (101,'impresora','Epson Stylus C45',400.80,20);

insert into articulos
values (203,'impresora','Epson Stylus C85',500,30);

insert into articulos
values (205,'monitor','Samsung 14',800,10);

insert into articulos
values (300,'teclado','ingles Biswal',100,50);
```


# El comercio quiere aumentar los precios de todos sus artículos en un 15%. Actualice todos los precios empleando operadores aritméticos.
codigo
```sql
update articulos
set precio = precio * 1.15;

```


# Vea el resultado.
codigo
```sql
select * from articulos;

```
salida
```sql
CODIGO  NOMBRE      DESCRIPCION         PRECIO   CANTIDAD
--------------------------------------------------------
101     impresora   Epson Stylus C45    460.92   20
203     impresora   Epson Stylus C85    575      30
205     monitor     Samsung 14          920      10
300     teclado     ingles Biswal       115      50


```

# Muestre todos los artículos, concatenando el nombre y la descripción de cada uno de ellos separados por coma.
codigo
```sql
select codigo, nombre || ', ' || descripcion as nombre_descripcion
from articulos;

```
salida
```sql
CODIGO  NOMBRE_DESCRIPCION
-------------------------------
101     impresora, Epson Stylus C45
203     impresora, Epson Stylus C85
205     monitor, Samsung 14
300     teclado, ingles Biswal


```

# Reste a la cantidad de todas las impresoras, el valor 5, empleando el operador aritmético menos ("-")
codigo
```sql
update articulos
set cantidad = cantidad - 5
where nombre = 'impresora';

```


# Recupere todos los datos de las impresoras para verificar que la actualización se realizó.
codigo
```sql
select * from articulos
where nombre = 'impresora';

```
salida
```sql
CODIGO  NOMBRE      DESCRIPCION         PRECIO   CANTIDAD
--------------------------------------------------------
101     impresora   Epson Stylus C45    460.92   15
203     impresora   Epson Stylus C85    575      30


```

# Muestre todos los artículos concatenado los campos para que aparezcan de la siguiente manera "Cod. 101: impresora Epson Stylus C45 $460,92 (15)"
codigo
```sql
select 'Cod. ' || codigo || ': ' || nombre || ' ' || descripcion || ' $' || precio || ' (' || cantidad || ')' as articulo_completo
from articulos;

```
salida
```sql
ARTICULO_COMPLETO
--------------------------------------------------------
Cod. 101: impresora Epson Stylus C45 $460.92 (15)
Cod. 203: impresora Epson Stylus C85 $575 (30)
Cod. 205: monitor Samsung 14 $920 (10)
Cod. 300: teclado ingles Biswal $115 (50)


```