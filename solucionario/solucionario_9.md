## Para aclarar algunas instrucciones, en ocasiones, necesitamos agregar comentarios.Es posible ingresar comentarios en la línea de comandos, es decir, un texto que no se ejecuta; para ello se emplean dos guiones (--):select *from libros;--mostramos los registros de libros en la línea anterior, todo lo que está luego de los guiones (hacia la derecha) no se ejecuta.Para agregar varias líneas de comentarios, se coloca una barra seguida de un asterisco (/) al comienzo del bloque de comentario y al finalizarlo, un asterisco seguido de una barra (/)

codigo sql
```sql
 select titulo, autor 
 /*mostramos títulos y
 nombres de los autores*/
 from libros;

Todo lo que está entre los símbolos "/" y "/" no se ejecuta.