# Ejercicio 01
# Un videoclub que alquila películas en video almacena la información de sus películas en una tabla llamada "peliculas"; para cada película necesita los siguientes datos:



codigo
```sql
 -nombre, cadena de caracteres de 20 de longitud,
 -actor, cadena de caracteres de 20 de longitud,
 -duración, valor numérico entero que no supera los 3 dígitos.
 -cantidad de copias: valor entero de un sólo dígito (no tienen más de 9 copias de cada película).
```



# Elimine la tabla "peliculas" si ya existe.

codigo
```sql
DROP TABLE peliculas;

```

salida
```sql
Tabla PELICULAS eliminada exitosamente.


```
# Cree la tabla eligiendo el tipo de dato adecuado para cada campo.

codigo
```sql
CREATE TABLE peliculas (
    nombre VARCHAR2(20),
    actor VARCHAR2(20),
    duracion NUMBER(3, 0),
    cantidad NUMBER(1, 0)
);

```

salida
```sql
Tabla PELICULAS creada exitosamente.


```

# Note que los campos "duracion" y "cantidad", que almacenarán valores sin decimales, fueron definidos de maneras diferentes, en el primero especificamos el valor 0 como cantidad de decimales, en el segundo no especificamos cantidad de decimales, es decir, por defecto, asume el valor 0.

# Vea la estructura de la tabla.

codigo
```sql
DESCRIBE peliculas;

```

salida
```sql
Name       Null?    Type
---------- -------- --------------
NOMBRE              VARCHAR2(20)
ACTOR               VARCHAR2(20)
DURACION            NUMBER(3)
CANTIDAD            NUMBER(1)


```

# Ingrese los siguientes registros:
codigo
```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',128,3);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',130,2);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mujer bonita','Julia Roberts',118,3);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',110,2);
```

# Muestre todos los registros (4 registros)
codigo
```sql
SELECT * FROM peliculas;

```

salida
```sql
NOMBRE              ACTOR             DURACION   CANTIDAD
------------------- ---------------- ---------- --------
Mision imposible    Tom Cruise              128          3
Mision imposible 2  Tom Cruise              130          2
Mujer bonita        Julia Roberts           118          3
Elsa y Fred         China Zorrilla          110          2


```

# Intente ingresar una película con valor de cantidad fuera del rango permitido:

codigo
```sql
INSERT INTO peliculas (nombre, actor, duracion, cantidad) VALUES ('Mujer bonita', 'Richard Gere', 1200, 10);

```

salida
```sql
Error: ORA-01438: valor numérico no válido


```

# Mensaje de error.

codigo
```sql
INSERT INTO peliculas (nombre, actor, duracion, cantidad) VALUES ('Mujer bonita', 'Richard Gere', 120.20, 4);

```

# Ingrese un valor con decimales en un nuevo registro, en el campo "duracion":

codigo
```sql
INSERT INTO peliculas (nombre, actor, duracion, cantidad) VALUES ('Mujer bonita', 'Richard Gere', 120.20, 4);

``` 



# Muestre todos los registros para ver cómo se almacenó el último registro ingresado.
codigo
```sql
SELECT * FROM peliculas;

```

salida
```sql
NOMBRE              ACTOR             DURACION   CANTIDAD
------------------- ---------------- ---------- --------
Mision imposible    Tom Cruise              128          3
Mision imposible 2  Tom Cruise              130          2
Mujer bonita        Julia Roberts           118          3
Elsa y Fred         China Zorrilla          110          2
Mujer bonita        Richard Gere           120.2         4


```

# Intente ingresar un nombre de película que supere los 20 caracteres.
codigo
```sql
INSERT INTO peliculas (nombre, actor, duracion, cantidad) VALUES ('Esta película tiene un nombre muy largo', 'Actor', 90, 1);

```

salida
```sql
Error: ORA-12899: el valor es demasiado largo para la columna "NOMBRE" (máximo: 20)


```