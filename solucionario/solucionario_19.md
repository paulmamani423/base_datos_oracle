# Ejercicios propuestos
# Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla con ese nombre.

# Elimine la tabla:
codigo
```sql
drop table articulos;
```


# Cree la tabla:
codigo
```sql

create table articulos(
    codigo number(4),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(8,2),
    cantidad number(3) default 0,
    primary key (codigo)
);

```


# Ingrese algunos registros:
codigo
```sql
insert into articulos
values (101,'impresora','Epson Stylus C45',400.80,20);

insert into articulos
values (203,'impresora','Epson Stylus C85',500,30);

insert into articulos
values (205,'monitor','Samsung 14',800,10);

insert into articulos
values (300,'teclado','ingles Biswal',100,50);
```

# El comercio hace un descuento del 15% en ventas mayoristas. Necesitamos recuperar el código, nombre, decripción de todos los artículos con una columna extra que muestre el precio de cada artículo para la venta mayorista con el siguiente encabezado "precio mayorista"
codigo
```sql
select codigo, nombre, descripcion, precio * 0.85 as "precio mayorista"
from articulos;

```
salida
```sql
CODIGO  NOMBRE      DESCRIPCION         precio mayorista
--------------------------------------------------------
101     impresora   Epson Stylus C45    340.68
203     impresora   Epson Stylus C85    425
205     monitor     Samsung 14          680
300     teclado     ingles Biswal       85


```
# Muestre los precios de todos los artículos, concatenando el nombre y la descripción con el encabezado "artículo" (sin emplear "as" ni comillas)
codigo
```sql
select 'artículo ' || nombre || ' ' || descripcion
from articulos;

```
salida
```sql
artículo impresora Epson Stylus C45
artículo impresora Epson Stylus C85
artículo monitor Samsung 14
artículo teclado ingles Biswal


```

# Muestre todos los campos de los artículos y un campo extra, con el encabezado "monto total" en la que calcule el monto total en dinero de cada artículo (precio por cantidad)
codigo
```sql
select *, precio * cantidad as "monto total"
from articulos;

```
salida
```sql
CODIGO  NOMBRE      DESCRIPCION         PRECIO   CANTIDAD   monto total
-----------------------------------------------------------------------
101     impresora   Epson Stylus C45    400.80   20          8016
203     impresora   Epson Stylus C85    500      30          15000
205     monitor     Samsung 14          800      10          8000
300     teclado     ingles Biswal       100      50          5000


```

# Muestre la descripción de todas las impresoras junto al precio con un 20% de recargo con un encabezado que lo especifique.
codigo
```sql
select descripcion || ' (20% recargo), $' || (precio * 1.2) as "Descripción y Precio"
from articulos
where nombre = 'impresora';

```
salida
```sql
Descripción y Precio
---------------------------------
Epson Stylus C45 (20% recargo), $480.96
Epson Stylus C85 (20% recargo), $600


```