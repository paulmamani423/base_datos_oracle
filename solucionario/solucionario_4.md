# Solucionario - Recuperar algunos campos (select)

## Practica de laboratorio

### creamos la base de datos "libros"


Codigo sql

```sql
drop table if exists libros;

create table libros(
  titulo varchar(20),
  autor varchar(30),
  editorial varchar(15),
  precio float,
  cantidad integer
);
```
Salida consulta

```sh
*Cause:    
*Action:

Table LIBROS creado
```