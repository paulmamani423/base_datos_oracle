# Ejercicios propuestos
# En una página web se guardan los siguientes datos de las visitas: nombre, mail, pais y fecha.

# Elimine la tabla "visitas" y créela con la siguiente estructura:
codigo
```sql
drop table visitas;

```
codigo
```sql
create table visitas (
    nombre varchar2(30) default 'Anonimo',
    mail varchar2(50),
    pais varchar2(20),
    fecha date
);

```



Ingrese algunos registros:

codigo
```sql
insert into visitas
values ('Ana Maria Lopez','<AnaMaria@hotmail.com>','Argentina',to_date('2020/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Gustavo Gonzalez','<GustavoGGonzalez@hotmail.com>','Chile',to_date('2020/02/13 11:08:10', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina',to_date('2020/07/02 14:12:00', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Fabiola Martinez','<MartinezFabiola@hotmail.com>','Mexico',to_date('2020/06/17 20:00:00', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Fabiola Martinez','<MartinezFabiola@hotmail.com>','Mexico',to_date('2020/02/08 20:05:40', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina',to_date('2020/07/06 18:00:00', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina',to_date('2019/10/05 23:00:00', 'yyyy/mm/dd hh24:mi:ss'));
```



Ordene los registros por fecha, en orden descendente.
codigo
```sql
select *
from visitas
order by fecha desc;
```
salida
```sql
NOMBRE              MAIL                           PAIS         FECHA
------------------- ------------------------------ ------------ -------------------
Juancito            <JuanJosePerez@hotmail.com>    Argentina    2020-07-06 18:00:00
Ana Maria Lopez     <AnaMaria@hotmail.com>          Argentina    2020-05-03 21:02:44
Fabiola Martinez    <MartinezFabiola@hotmail.com>   Mexico       2020-06-17 20:00:00
Juancito            <JuanJosePerez@hotmail.com>    Argentina    2020-07-02 14:12:00
Fabiola Martinez    <MartinezFabiola@hotmail.com>   Mexico       2020-02-08 20:05:40
Gustavo Gonzalez    <GustavoGGonzalez@hotmail.com> Chile        2020-02-13 11:08:10
Juancito            <JuanJosePerez@hotmail.com>    Argentina    2019-10-05 23:00:00


```

Muestre el nombre del usuario, pais y el mes, ordenado por pais (ascendente) y el mes (descendente)
codigo
```sql
select nombre, pais, to_char(fecha, 'Month') as mes
from visitas
order by pais asc, mes desc;
```
salida
```sql
NOMBRE              PAIS         MES
------------------- ------------ ---------
Juancito            Argentina    July
Ana Maria Lopez     Argentina    May
Juancito            Argentina    July
Fabiola Martinez    Mexico       June
Fabiola Martinez    Mexico       February
Gustavo Gonzalez    Chile        February
Juancito            Argentina    October


```

Muestre los mail, país, ordenado por país, de todos los que visitaron la página en octubre (4 registros)
codigo
```sql
select mail, pais
from visitas
where to_char(fecha, 'MM') = '10'
order by pais;
```
salida
```sql
MAIL                           PAIS
------------------------------ ------------
<JuanJosePerez@hotmail.com>    Argentina
<JuanJosePerez@hotmail.com>    Argentina
<JuanJosePerez@hotmail.com>    Argentina
<MartinezFabiola@hotmail.com>  Mexico


```