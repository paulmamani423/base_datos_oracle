# Ejercicio 01
# Trabaje con la tabla llamada "medicamentos" de una farmacia.

# Elimine la tabla y créela con la siguiente estructura:
codigo
```sql
drop table medicamentos;
```
codigo
```sql
create table medicamentos(
    codigo number(5),
    nombre varchar2(20),
    laboratorio varchar2(20),
    precio number(5,2),
    cantidad number(3),
    primary key(codigo)
);


```



# Ingrese algunos registros:

codigo
```sql
insert into medicamentos
values(100,'Sertal','Roche',5.2,100);

insert into medicamentos
values(102,'Buscapina','Roche',4.10,200);

insert into medicamentos
values(205,'Amoxidal 500','Bayer',15.60,100);

insert into medicamentos
values(230,'Paracetamol 500','Bago',1.90,200);

insert into medicamentos
values(345,'Bayaspirina','Bayer',2.10,150);

insert into medicamentos
values(347,'Amoxidal jarabe','Bayer',5.10,250);

```

# Recupere los códigos y nombres de los medicamentos cuyo laboratorio sea "Roche' y cuyo precio sea menor a 5 (1 registro cumple con ambas condiciones)
codigo
```sql
SELECT codigo, nombre
FROM medicamentos
WHERE laboratorio = 'Roche' AND precio < 5;
```
salida
```sql

CODIGO | NOMBRE
--------------
100    | Sertal

```

# Recupere los medicamentos cuyo laboratorio sea "Roche" o cuyo precio sea menor a 5 (4 registros)
codigo
```sql
SELECT *
FROM medicamentos
WHERE laboratorio = 'Roche' OR precio < 5;

```
salida
```sql
CODIGO | NOMBRE           | LABORATORIO | PRECIO | CANTIDAD
----------------------------------------------------------
100    | Sertal           | Roche       | 5.2    | 100
102    | Buscapina        | Roche       | 4.1    | 200
230    | Paracetamol 500  | Bago        | 1.9    | 200
345    | Bayaspirina      | Bayer       | 2.1    | 150


```

# Muestre todos los medicamentos cuyo laboratorio NO sea "Bayer" y cuya cantidad sea=100. Luego muestre todos los medicamentos cuyo laboratorio sea "Bayer" y cuya cantidad NO sea=100
codigo
```sql
SELECT *
FROM medicamentos
WHERE laboratorio <> 'Bayer' AND cantidad = 100;

SELECT *
FROM medicamentos
WHERE laboratorio = 'Bayer' AND cantidad <> 100;

```
salida
```sql
CODIGO | NOMBRE           | LABORATORIO | PRECIO | CANTIDAD
----------------------------------------------------------
205    | Amoxidal 500     | Bayer       | 15.6   | 100
345    | Bayaspirina      | Bayer       | 2.1    | 150
347    | Amoxidal jarabe  | Bayer       | 5.1    | 250

```

# Recupere los nombres de los medicamentos cuyo precio esté entre 2 y 5 inclusive (2 registros)
codigo
```sql
SELECT nombre
FROM medicamentos
WHERE precio BETWEEN 2 AND 5;

```
salida
```sql
NOMBRE
--------------
Sertal
Buscapina
Bayaspirina
Amoxidal jarabe


```

# Elimine todos los registros cuyo laboratorio sea igual a "Bayer" y su precio sea mayor a 10 (1 registro eliminado)
codigo
```sql
DELETE FROM medicamentos
WHERE laboratorio = 'Bayer' AND precio > 10;

```
salida
```sql
CODIGO | NOMBRE           | LABORATORIO | PRECIO | CANTIDAD
----------------------------------------------------------
100    | Sertal           | Roche       | 5.2    | 100
102    | Buscapina        | Roche       | 4.1    | 200
230    | Paracetamol 500  | Bago


```

# Cambie la cantidad por 200, de todos los medicamentos de "Roche" cuyo precio sea mayor a 5 (1 registro afectado)
codigo
```sql
UPDATE medicamentos
SET cantidad = 200
WHERE laboratorio = 'Roche' AND precio > 5;

```
salida
```sql


```

# Muestre todos los registros para verificar el cambio.

codigo
```sql
SELECT *
FROM medicamentos;

```
salida
```sql
CODIGO | NOMBRE           | LABORATORIO | PRECIO | CANTIDAD
----------------------------------------------------------
100    | Sertal           | Roche       | 5.2    | 100
102    | Buscapina        | Roche       | 4.1    | 200
205    | Amoxidal 500     | Bayer       | 15.6   | 100
230    | Paracetamol 500  | Bago        | 1.9    | 200
345    | Bayaspirina      | Bayer       | 2.1    | 150
347    | Amoxidal jarabe  | Bayer       | 5.1    | 250


```
# Borre los medicamentos cuyo laboratorio sea "Bayer" o cuyo precio sea menor a 3 (3 registros borrados)
codigo
```sql
DELETE FROM medicamentos
WHERE laboratorio = 'Bayer' OR precio < 3;

```
salida
```sql
CODIGO | NOMBRE           | LABORATORIO | PRECIO | CANTIDAD
----------------------------------------------------------
100    | Sertal           | Roche       | 5.2    | 100
102    | Buscapina        | Roche       | 4.1    | 200
230    | Paracetamol 500  | Bago


```