# Ejercicio 01
# Trabaje con la tabla "libros" de una librería.

# Elimine la tabla:
codigo
```sql
drop table libros;

```
salida
```sql
Tabla "libros" eliminada correctamente.

```

# Créela con los siguientes campos, estableciendo como clave primaria el campo "codigo":

codigo
```sql
create table libros(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    autor varchar2(20),
    editorial varchar2(15),
    primary key (codigo)
);

```
salida
```sql

Tabla "libros" creada exitosamente.
```



# Ingrese los siguientes registros:
codigo
```sql
insert into libros (codigo,titulo,autor,editorial) values (1,'El aleph','Borges','Emece');
insert into libros (codigo,titulo,autor,editorial) values (2,'Martin Fierro','Jose Hernandez','Planeta');
insert into libros (codigo,titulo,autor,editorial) values (3,'Aprenda PHP','Mario Molina','Nuevo Siglo');
```
salida
```sql
Registros insertados correctamente.

```

# Ingrese un registro con código repetido (aparece un mensaje de error)

codigo
```sql
INSERT INTO libros (codigo, titulo, autor, editorial) VALUES (1, 'Otro libro', 'Otro autor', 'Otra editorial');     
```
salida
```sql
Error: Violación de restricción de clave primaria. El valor de código ya existe en la tabla.

```
# Intente ingresar el valor "null" en el campo "codigo"
codigo
```sql
INSERT INTO libros (codigo, titulo, autor, editorial) VALUES (NULL, 'Libro sin código', 'Autor', 'Editorial');
```


# Intente actualizar el código del libro "Martin Fierro" a "1" (mensaje de error)
codigo
```sql
UPDATE libros SET codigo = 1 WHERE titulo = 'Martin Fierro';
```

# Actualice el código del libro "Martin Fierro" a "10"
codigo
```sql
UPDATE libros SET codigo = 10 WHERE titulo = 'Martin Fierro';
```

# Vea qué campo de la tabla "LIBROS" fue establecido como clave primaria
codigo
```sql
SELECT COLUMN_NAME
FROM ALL_CONSTRAINTS
JOIN ALL_CONS_COLUMNS ON ALL_CONSTRAINTS.CONSTRAINT_NAME = ALL_CONS_COLUMNS.CONSTRAINT_NAME
WHERE UPPER(TABLE_NAME) = 'LIBROS' AND CONSTRAINT_TYPE = 'P';
```
salida
```sql
COLUMN_NAME
-------------
CODIGO

```

# Vea qué campo de la tabla "libros" (en minúsculas) fue establecido como clave primaria
codigo
```sql
SELECT COLUMN_NAME
FROM ALL_CONSTRAINTS
JOIN ALL_CONS_COLUMNS ON ALL_CONSTRAINTS.CONSTRAINT_NAME = ALL_CONS_COLUMNS.CONSTRAINT_NAME
WHERE UPPER(TABLE_NAME) = 'LIBROS' AND UPPER(CONSTRAINT_NAME) = 'SYS_C007900';

```
salida
```sql
 COLUMN_NAME
-------------
codigo

```

# La tabla aparece vacía porque Oracle no encuentra la tabla "libros", ya que almacena los nombres de las tablas con mayúsculas.
codigo
```sql
SELECT * FROM libros;
```
